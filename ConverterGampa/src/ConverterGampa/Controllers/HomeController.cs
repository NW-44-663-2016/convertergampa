﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterGampa.Models;


namespace ConverterGampa.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Gampa";
            ViewData["Result"] = "";
            ViewData["Value"] = "";

            Converter converter = new Converter();
            return View(converter);
        }

        public IActionResult Convert(Converter converter)
        {
            double result;
            if (ModelState.IsValid)
            {
                result = (converter.Temperature_F - 32) * 5.0 / 9.0;
                ViewData["Title"] = "Converted by Gampa";
                ViewData["Value"] = result;
                ViewData["Result"] = "Temperature in C = " +
                    (int)(result);
            }
            return View("Index", converter);
        }

    }
}

